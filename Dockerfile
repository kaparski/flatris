FROM node

RUN mkdir /code

COPY . /code

WORKDIR /code

RUN yarn install

RUN yarn build

CMD yarn start
